import * as React from 'react'
import { RouterProps } from 'react-router'
import { Switch, Redirect, Route } from 'react-router-dom'

import MainLayout from './layouts/MainLayout'
import { ProductList } from './pages/ProductList'
import {Settings} from "./pages/Settings";
import {ProductDetail} from "./pages/ProductDetail";

interface IRoute {
  exact?: boolean
  layout?: React.FC
  guard?: React.FC
  path?: string
  component: React.FC<RouterProps>
  routes?: IRoute[]
}

export const renderRoutes = (routes: IRoute[] = []): React.ReactNode => (
  <React.Suspense fallback={<>Loading</>}>
    <Switch>
      {routes.map((route, i) => {
        const Layout = route.layout || React.Fragment
        const Component = route.component

        return (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            render={(props) => (
              <Layout>
                {route.routes ? (
                  renderRoutes(route.routes)
                ) : (
                  <Component {...props} />
                )}
              </Layout>
            )}
          />
        )
      })}
    </Switch>
  </React.Suspense>
)

export const routes: IRoute[] = [
  {
    exact: true,
    layout: MainLayout,
    path: '/',
    component: ProductList
  },
  {
    exact: true,
    layout: MainLayout,
    path: '/settings',
    component: Settings
  },
  {
    exact: true,
    layout: MainLayout,
    path: '/products/:id',
    component: ProductDetail
  },
  {
    component: () => <Redirect to="/" />
  }
]
