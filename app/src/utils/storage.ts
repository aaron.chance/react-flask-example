const STORAGE = localStorage
const TOKEN = 'token'

export default class Storage {

  static clearAll(): void {
    STORAGE.clear()
  }

  static get(name: string): string | null {
    return STORAGE.getItem(name)
  }

  static set(name: string, value: string): void {
    STORAGE.setItem(name, value)
  }

  static remove(name: string): void {
    STORAGE.removeItem(name)
  }

}
