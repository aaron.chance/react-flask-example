import axiosClient from './axiosClient'
import {
  IGetProductsResponse,
  IGetProductResponse,
  IProduct
} from '../interfaces/IProducts'
import {IMessage, IMessageResponse} from '../interfaces/IMessages'

export const getProducts = async (): Promise<IProduct[]> => {
  const {data: { success, products }}: { data: IGetProductsResponse } = await axiosClient.get('/products')
  if (success) {
    return products
  }
  throw new Error('Error while fetching product list')
}

export const getProductDetails = async (productID: string): Promise<IProduct> => {
  const {data: { success, product }}: { data: IGetProductResponse} = await axiosClient.get(`/products/${productID}`)
  if (success) {
    return product
  }
  throw new Error(`Error while fetching product with ID ${productID}`)
}

export const createMessage = async (form_data: IMessage): Promise<IMessage> => {
  const {data: { success, message }}: { data: IMessageResponse} = await axiosClient.post('/messages', form_data)
  if (success) {
    return message
  }
  throw new Error('Error creating new message')

}

export const updateMessage = async (form_data: IMessage, message_id: any): Promise<IMessage> => {
  const {data: { success, message }}: { data: IMessageResponse} = await axiosClient.put(`/messages/${message_id}`, form_data)
  if (success) {
    return message
  }
  throw new Error('Error creating new message')
}