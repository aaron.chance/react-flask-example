import axios from 'axios'
import { API_BASE_URL } from './constants'
import Storage from './storage'
const createAxiosClient = () => {
  const instance = axios.create({
    baseURL: API_BASE_URL
  })
  instance.interceptors.request.use((config) => {
    try {
      config.headers.Accept = 'application/json,text/plain'
      config.headers['Content-Type'] = 'application/json'
      config.headers['Cache-Control'] = 'no-cache'

      return config
    } catch (err) {
      // return original config if JSON.parse fails
      return config
    }
  })

  return instance
}

export default createAxiosClient()
