// When running the API Locally this can be the API's ngrok tunnel address or the localhost: + process.env.PORT
export const API_BASE_URL = 'http://localhost:5000'

export const SETTINGS = [
  {
    label: 'Create Product',
    route: '/product/create'
  },
]
