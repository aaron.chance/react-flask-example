import { IMessage } from './IMessages';

export interface IGetProductsResponse {
  success: boolean
  products: IProduct[]
}
export interface IGetProductResponse {
  success: boolean
  product: IProduct
}

export interface IProduct {
  id: number
  name: string
  description: string
  price: number
  sale_price: number
  messages?: IMessage[]
}

