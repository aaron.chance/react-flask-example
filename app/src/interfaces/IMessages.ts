export interface IMessage {
  id?: number
  product_id?: number
  body: string
  is_sent: boolean
  send_to?: string
  created_at?: Date
  updated_at?: Date
}

export interface IMessageResponse {
  success: boolean
  message: IMessage
}