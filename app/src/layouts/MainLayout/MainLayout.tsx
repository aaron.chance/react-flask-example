import * as React from 'react'
import { useLocation, useHistory } from 'react-router-dom'

import NavBar from '../../components/NavBar'
import Container from '../../components/Container'
import './style.scss'
import Button from "../../components/Button";

const MainLayout: React.FC = ({ children }) => {
  const history = useHistory()

  const goToSettings = () => {
    history.push('/settings')
  }
  return (
    <div className="main-layout-container">
      <NavBar>
        <div className="nav-bar-title-wrapper">
          <div className="nav-bar-title">React/Flask Sample App</div>
        </div>
        <div className="action-button-wrapper">
          <Button
            variant="primary"
            label={'Settings'}
            onClick={goToSettings}
          />
        </div>
      </NavBar>
      <Container>
        <div className="content">{children}</div>
      </Container>
    </div>
  )
}

export default MainLayout
