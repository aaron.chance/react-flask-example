import * as React from 'react'

interface TextareaProps
  extends React.ButtonHTMLAttributes<HTMLTextAreaElement> {
  label?: string
  helper?: string
  marker?: string
}

const Textarea: React.FC<TextareaProps> = ({
  label,
  helper,
  marker,
  ...props
}) => (
  <div className="rfs-form-field-container">
    {label && (
      <div className="rfs-form-label-container">
        <label className="rfs-form-label" htmlFor="field-textarea">
          {label}
        </label>
        {marker && (
          <span
            aria-describedby="field-textarea"
            className="rfs-form-label-secondary"
          >
            {marker}
          </span>
        )}
      </div>
    )}
    <textarea
      id="field-textarea"
      className="rfs-form-field"
      type="text"
      {...props}
    />
    {helper && (
      <p aria-describedby="field-textarea" className="rfs-form-description">
        {helper}
      </p>
    )}
  </div>
)

export default Textarea
