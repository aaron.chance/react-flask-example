import * as React from 'react'
import { Field } from 'formik'
import {useState} from "react";
import {bool} from "yup";

interface FTextareaProps {
  name: string
  label?: string
  helper?: string
  marker?: string
  max_chars?: number
  disabled?: boolean
  placeholder?: string
}

const Textarea: React.FC<FTextareaProps> = ({
  name,
  label,
  helper,
  marker,
  disabled = false,
  placeholder,
}) => {
  return (
  <Field name={name}>
    {({ field }: { field: typeof Field }) => (
      <div className="rfs-form-field-container">
        {label && (
          <div className="rfs-form-label-container">
            <label className="rfs-form-label" htmlFor="field-textarea">
              {label}
            </label>
            {marker && (
              <span
                aria-describedby="field-textarea"
                className="rfs-form-label-secondary"
              >
                {marker}
              </span>
            )}
          </div>
        )}
        <textarea
          className="rfs-form-field"
          placeholder={placeholder}
          disabled={disabled}
          {...field}
        />
        {helper && (
          <p
            aria-describedby="field-textarea"
            className="rfs-form-description"
          >
            {helper}
          </p>
        )}
      </div>
    )}
  </Field>
)}

export default Textarea
