import * as React from 'react'
import { Field } from 'formik'

interface FInputProps {
  name: string
  type?: string
  label?: string
  value?: string | number
  helper?: string
  marker?: string
  placeholder?: string
}

const FInput: React.FC<FInputProps> = ({
  name,
  value= '',
  label,
  helper,
  marker,
  placeholder,
  type = 'text'
}) => (
  <Field name={name}>
    {({ field }: { field: typeof Field }) => (
      <div className="rfs-form-field-container">
        {label && (
          <div className="rfs-form-label-container">
            <label className="rfs-form-label" htmlFor="field-input">
              {label}
            </label>
            {marker && (
              <span
                aria-describedby="field-input"
                className="rfs-form-label-secondary"
              >
                {marker}
              </span>
            )}
          </div>
        )}
        <input
          className="rfs-form-field"
          placeholder={placeholder}
          type={type}
          value={value}
          {...field}
        />
        {helper && (
          <p aria-describedby="field-input" className="rfs-form-description">
            {helper}
          </p>
        )}
      </div>
    )}
  </Field>
)

export default FInput
