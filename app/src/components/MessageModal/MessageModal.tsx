import * as React from 'react'
import Modal from 'react-modal'
import { Formik, Form, getIn } from 'formik'
import * as Yup from 'yup'
import FInput from '../Formik/FInput'
import Container from '../Container'
import FTextarea from '../Formik/FTextarea'
import Button from '../Button'
import './style.scss'
import FSelect from '../Formik/FSelect'
import { IMessage } from '../../interfaces/IMessages'
import {createMessage, updateMessage} from '../../utils/api'

Modal.setAppElement('#root')

interface MessageModalProps {
  open: boolean
  is_editing: boolean
  onClose: () => void
  productId: number
  message?: IMessage
}

const messageSchema = Yup.object().shape({
  send_to: Yup.string()
    .min(10, 'Too Short!')
    .required('A phone number is required'),
  body: Yup.string()
    .min(2, 'Too Short!')
    .max(160, 'Too Long!')
    .required('A value is required')
})

const MessageModal: React.FC<MessageModalProps> = ({
  open,
  is_editing,
  onClose,
  productId,
  message
}) => {
  const [submissionError, setSubmissionError] = React.useState<boolean | string>(false)
  const statusOptions = [
    {
      label: 'Save As Draft', value: 'false',
    },
    {
      label: 'Send Immediately', value: 'true'
    }
  ]
  const bodyTemplateVars = [
    {
      label: '', value: '{price}',
    }
  ]
  const initialValues: any = {
    body: message ? message.body : '',
    send_to: '',
    is_sent: message ? message.is_sent : false,
    product_id: productId
  }
  return (
    <Modal
      isOpen={open}
      contentLabel={is_editing ? 'Send Message' : 'Create Message'}
      onRequestClose={onClose}
      className="message-modal"
    >
      <Container>
        <div className="content-container">
          <h1>{is_editing ? 'Send' : 'Create'} Message</h1>
          <Formik
            initialValues={initialValues}
            validationSchema={messageSchema}
            onSubmit={async (values, { setSubmitting }) => {
              setSubmissionError(false)
              if(is_editing){
                values.is_sent = 1
                await updateMessage(values, message?.id).then((res: IMessage) => {
                  setSubmitting(false)
                  onClose()
                })
                  .catch((err) => {
                    setSubmissionError(err.message)
                    setSubmitting(false)
                  })
              }
              else {
                await createMessage(values).then((res: IMessage) => {
                  setSubmitting(false)
                  onClose()
                })
                  .catch((err) => {
                    setSubmissionError(err.message)
                    setSubmitting(false)
                  })
              }
            }}
          >
            {({
              errors,
              touched,
              isSubmitting,
              isValid
            }: {
              errors: any
              touched: any
              isSubmitting: boolean
              isValid: boolean
            }) => (
              <Form>
                <FInput label="To Number" name="send_to" />
                {errors.send_to && touched.send_to ? (
                  <p className={'invalid-error'}>{errors.send_to}</p>
                ) : null}

                <FInput type="hidden" name="product_id" value={productId} />

                <FTextarea
                  name="body"
                  label="Message body"
                  disabled={is_editing}
                />
                {errors.body && touched.body ? (
                  <p className={'invalid-error'}>
                    {errors.body}
                  </p>
                ) : null}

                {!is_editing && (
                  <>
                    <FSelect
                      label="Status"
                      name="is_sent"
                      options={statusOptions}
                    />
                  </>
                )}
                <div className="controls-container">
                  <Button variant="tertiary" label="Cancel" onClick={onClose} />
                  <Button
                    variant="primary"
                    label="Save"
                    type="submit"
                    disabled={!isValid || isSubmitting}
                  />
                  {submissionError && (
                    <p className={'invalid-error'}>{submissionError}</p>
                  )}
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </Modal>
  )
}

export default MessageModal
