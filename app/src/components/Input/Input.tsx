import * as React from 'react'

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  label?: string
  helper?: string
  marker?: string
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ label, helper, marker, ...props }, ref) => (
    <div className="rfs-form-field-container">
      {label && (
        <div className="rfs-form-label-container">
          <label className="rfs-form-label" htmlFor="field-input">
            {label}
          </label>
          {marker && (
            <span
              aria-describedby="field-input"
              className="rfs-form-label-secondary"
            >
              {marker}
            </span>
          )}
        </div>
      )}
      <input ref={ref} className="rfs-form-field" type="text" {...props} />
      {helper && (
        <p aria-describedby="field-input" className="rfs-form-description">
          {helper}
        </p>
      )}
    </div>
  )
)

export default Input
