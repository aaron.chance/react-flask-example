import * as React from 'react'
import { toast } from 'react-toastify'

import Container from '../../components/Container'
import { SettingItem } from './SettingItem'
import './style.scss'
import Storage from '../../utils/storage'
import { SETTINGS } from '../../utils/constants'

const Settings: React.FC = () => {
  const [error, setError] = React.useState<boolean | string>(false)

  return (
    <Container>
      <div className="settings-container">
        <div className="page-title">Manage Store</div>
        <div className="settings">
          {SETTINGS.map((item, index) => (
            <SettingItem key={index} {...item} />
          ))}
        </div>
      </div>
    </Container>
  )
}

export { Settings }
