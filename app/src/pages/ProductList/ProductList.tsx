import * as React from 'react'
import { Link } from "react-router-dom";
import Button from '../../components/Button'
import './style.scss'
import { IProduct } from '../../interfaces/IProducts'
import { getProducts } from '../../utils/api'

const ProductList: React.FC = () => {
  const [products, setProducts] = React.useState<IProduct[]>([])

  React.useEffect(() => {
    getProducts().then((response: IProduct[]) => {
      console.log(response)
      setProducts(response)
    })
  }, [])

  return (
    <div className="auth-page-container">
      <div className="page-title">Product List</div>
      <div className="logo-container">
        {
        products.map(product => {
          return (
            <div className="card" key={product.id}>
              <div className="title"><b>{product.name}</b></div>
              <div className="price-container">
                <span className={"price" + (product.sale_price ? " strike" : "") }>${product.price}</span>&nbsp;
              {product.sale_price && <span className="sale-price">${product.sale_price}</span>}
              </div>
              <Link to={"/products/" + product.id}>
                <Button className="view-detail-button" variant="primary" label="View Details" />
              </Link>
            </div>
          )
        })
      }
      </div>
    </div>
  )
}

export { ProductList }
