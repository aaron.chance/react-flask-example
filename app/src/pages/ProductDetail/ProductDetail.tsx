import * as React from 'react'
import {useParams} from 'react-router'
import './style.scss'
import {IProduct} from '../../interfaces/IProducts'
import {getProductDetails} from '../../utils/api'
import {IMessage} from "../../interfaces/IMessages";
import MessageModal from "../../components/MessageModal";
import Button from "../../components/Button";

const ProductDetail: React.FC = () => {
  const [openModal, setOpenModal] = React.useState(false)
  const [isEditing, setIsEditing] = React.useState(false)
  const [product, setProduct] = React.useState<IProduct | undefined>(undefined)
  const [messages, setMessages] = React.useState<IMessage[] | []>([])
  const [selectedMessage, setSelectedMessage] = React.useState<IMessage | undefined>(undefined)
  const params: any  = useParams()

  React.useEffect(() => {
    console.log(params)
    getProductDetails(params.id).then((product) => {
      setProduct(product)
      if(product?.messages)
        setMessages(product.messages)
    })
  }, [])

  React.useEffect(() => {
  }, [])

  const renderRows = () => {
    if (messages.length > 0) {
      return messages.map((message: IMessage) => (
        <div className="table-row" key={message.id}>
          <div className="table-cell">{message.created_at }</div>
          <div className="table-cell">{message.is_sent ? 'Sent' : 'Draft' }</div>
          <div className="table-cell">{message.body}</div>
          <div className="table-cell">
            {!message.is_sent ?
              (<Button
              variant="secondary"
              label="Send Message"
              onClick={() => {
                setIsEditing(true)
                setSelectedMessage(message)
                setOpenModal(true)
              }}
            />) : ('')}
          </div>
        </div>
      ))
    } else {
      return (
        <div className="table-rows">
          <div className="table-cell">No records found</div>
        </div>
      )
    }
  }

  return (
    <div className="product-detail-container">
      <div className="page-title">
        <span>{product?.name}</span>
        <Button
          variant="secondary"
          label="Create a new Message"
          onClick={() => {
            setSelectedMessage(undefined)
            setIsEditing(false)
            setOpenModal(true)
          }}
        />

      </div>
      <div className="sub-page-title">
      </div>

      <div className="details-container">
        <div>Price: {product?.price}</div>
        <div>Sale Price: {product?.sale_price}</div>
        <div>Description: {product?.description}</div>
      </div>
      <div className="page-title">Messages created for this product</div>
      <div className="table messages-table">
        <div className="table-header">
          <div className="table-row">
            <div className="table-cell">Date</div>
            <div className="table-cell">Status</div>
            <div className="table-cell">Message</div>
            <div className="table-cell">Actions</div>
          </div>
        </div>
        <div className="table-body">{renderRows()}</div>
      </div>
      <MessageModal
        open={openModal}
        is_editing={isEditing}
        productId={params.id}
        message={selectedMessage}
        onClose={() => {
          setOpenModal(false)
          getProductDetails(params.id).then((product) => {
            setProduct(product)
            if(product?.messages)
              setMessages(product.messages)
          })
        }}
      />

    </div>
  )
}

export { ProductDetail }
