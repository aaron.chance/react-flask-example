import deleteIcon from './rfs-icons/delete.svg'
import tickCircleFillIcon from './rfs-icons/tick-circle-fill.svg'
import menuRightIcon from './rfs-icons/menu-right.svg'
import { ReactComponent as IntegrationIcon } from './rfs-icons/integration.svg'
import { ReactComponent as TickIcon } from './rfs-icons/tick.svg'
import { ReactComponent as CrossIcon } from './rfs-icons/cross.svg'

export {
  deleteIcon,
  tickCircleFillIcon,
  menuRightIcon,
  IntegrationIcon,
  TickIcon,
  CrossIcon
}
