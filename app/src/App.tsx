import * as React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { renderRoutes, routes } from './routes'
import './style/style.scss'
import { TickIcon } from './style/icon'

const App: React.FC = () => {

  return (
    <>
      <Router>{renderRoutes(routes)}</Router>
      <ToastContainer enableMultiContainer />
      <ToastContainer
        enableMultiContainer
        containerId='sync-toast'
        className="sync-notification-container"
        position="bottom-center"
        autoClose={false}
        closeButton={false}
      />
    </>
  );
}

export default App;
