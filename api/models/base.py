""" Initializes the model bases without mounting it to the Flask App. """


from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime


db = SQLAlchemy()
migrate = Migrate()


class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=False, unique=False, nullable=False)
    description = db.Column(db.String(280), index=False, unique=False, nullable=True)
    price = db.Column(db.Integer, index=False, unique=False, nullable=False, default='0')
    sale_price = db.Column(db.Integer, index=False, unique=False, nullable=False, default='0')
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_at = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    messages = db.relationship('Message', backref='products', cascade="all, delete-orphan")


class Message(db.Model):
    __tablename__ = 'messages'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(280), index=False, unique=False, nullable=True)
    status = db.Column(db.Integer, index=False, unique=False, nullable=False, default=0)
    send_to = db.Column(db.String(12), index=False, unique=False, nullable=True)
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_at = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
