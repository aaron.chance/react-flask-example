from utils.functions import convertToDollars


def productSchema(product, messages=None):
    schema = {
        'id': product.id,
        'name': product.name,
        'description': product.description,
        'price': convertToDollars(product.price),
        'sale_price': convertToDollars(product.sale_price)
    }
    if messages is not None and len(messages) >= 0:
        schema['messages'] = messages
    else:
        schema['messages'] = []
    return schema


def messageSchema(message):
    schema = {
        'id': message.id,
        'is_sent': message.status,
        'send_to': message.send_to,
        'body': message.body,
        'product_id': message.product_id,
        'updated_at': message.updated_at,
        'created_at': message.created_at
    }
    return schema
