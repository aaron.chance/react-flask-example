""" The main Flask application file that bootstraps and starts the app. """

import os

from bootstrap import app_factory, database_factory
from models.base import *
from models.ServiceError import ServiceError
from models.schemas import *
from flask import jsonify, request
from utils.functions import *
from flask_cors import CORS, cross_origin
app = app_factory()
# TODO: This doesn't have to be done here,
#       but should be done at some point to
#       mount SQLAlchemy to the Flask app.
db = database_factory(app)


@app.route("/health-check")
def health_check():
    return {"success": True}


@app.route('/products', methods=['POST'])
def create_product():
    response = {}
    data = request.get_json(True)
    # in a production case where I was taking more time to build this, I would use middleware or model-based validation
    # to ensure that data was present and handling the errors, rather than re-implementing in each method endpoint
    # handler
    if 'name' not in data:
        raise ServiceError('A product name is required', status_code=400)

    new_product = Product(name=data['name'], description=data['description'])
    if 'price' in data:
        new_product.price = convertToCents(data['price'])
    if 'sale_price' in data:
        new_product.sale_price = convertToCents(data['sale_price'])
    db.session.add(new_product)
    db.session.commit()
    response['success'] = True
    response['product'] = productSchema(new_product)
    return response


@app.route('/products', methods=['GET'])
def list_products():
    response = {}
    products = []

    results = Product.query.all()
    if len(results) <= 0:
        raise ServiceError('No results found', status_code=404)

    for product in results:
        products.append(productSchema(product))
    response['success'] = True
    response['products'] = products
    return response


@app.route('/products/<product_id>', methods=['GET'])
def get_product_details(product_id):
    response = {}
    messages = []

    product = Product.query.filter(Product.id == product_id).first()
    messages_results = Message.query.filter(Message.product_id == product_id).all()
    for message in messages_results:
        messages.append(messageSchema(message))
    response['success'] = True
    response['product'] = productSchema(product, messages)
    return response


@app.route('/messages', methods=['POST'])
def create_message():
    response = {}
    data = request.get_json(True)
    if 'product_id' not in data:
        raise ServiceError('Product ID is required', status_code=400)

    check_product_exists(data['product_id'])

    message = Message(body=data['body'], product_id=data['product_id'])

    if 'send_to' in data:
        message.send_to = data['send_to']

    db.session.add(message)
    db.session.commit()
    if 'status' in data:
        result = send_message(message)
        print(result)
    response['success'] = True
    response['message'] = messageSchema(message)
    return response


@app.route('/messages/<message_id>', methods=['PUT'])
def update_message(message_id):
    response = {}
    data = request.get_json(True)
    if 'product_id' not in data:
        raise ServiceError('Product ID is required', status_code=400)

    check_product_exists(data['product_id'])

    message = Message.query.filter(Message.id == message_id).first()
    if 'body' in data:
        message.body = data['body']
    if 'send_to' in data:
        message.send_to = data['send_to']
    if 'is_sent' in data:
        message.status = data['is_sent']
    db.session.commit()
    if data['is_sent'] and message.send_to is not None:
        response['twillio_sid'] = send_message(message)
    response['success'] = True
    response['message'] = messageSchema(message)
    return response


@app.route('/messages/<message_id>/send', methods=['GET'])
def send_selected_message(message_id):
    response = {}

    message = Message.query.filter(Message.id == message_id).first()

    if 'product_id' not in message:
        raise ServiceError('Product ID is required', status_code=400)

    check_product_exists(message.product_id)
    result = send_message(message)

    response['success'] = True
    response['twillio_sid'] = result
    response['message'] = messageSchema(message)
    return response


@app.errorhandler(ServiceError)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


if __name__ == "__main__":
    app.run(debug=os.environ.get("FLASK_DEBUG", False))
