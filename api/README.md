# Things to note

## Creating Products 

---
Due to running out of time, the only way to create a product is either manually via sql queries, or through the likes of
postman as a request to the api endpoint. The payload to create a product is as follows:

`{
    "name": "Product Name",
    "description": "Product description",
    "price": "7.50",
    "sale_price": "3.50"
}`

I've included methods when creating products that convert the dollars to cents when saving and then back to dollars
when returning the data in a response.

## Database setup

---
I could not figure out how to fully automate using flask-sqlalchemy and alembic to migrate the database when spinning
up the docker containers. You have two options available -- i've included the postgres-data folder which with my sparse 
testing should contain the database I created. Alternatively, you can `docker exec bash` into the api container and
manually run `flask db upgrade` to run all the migrations on a freshly created database.