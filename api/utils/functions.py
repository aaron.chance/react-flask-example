import os
from models.base import *
from models.ServiceError import ServiceError
from twilio.rest import Client

def convertToDollars(amountInCents):
    amount = float(round(amountInCents/100, 2))
    amount = '%.2f' % amount
    return amount


def convertToCents(amountInDollars):
    # converts dollar float to int
    amountInDollars = amountInDollars.replace('$', '')
    amount = int(round(float(amountInDollars)* 100, 2))
    return amount


def send_message(message):
    account_sid = os.environ['TWILIO_ACCOUNT_SID']
    auth_token = os.environ['TWILIO_AUTH_TOKEN']
    if message.send_to is None:
        message.send_to = "+18652336856"
        message.status = 1
        db.session.commit()
    client = Client(account_sid, auth_token)
    twilio_response = client.messages.create(
        to=message.send_to,
        from_="+19163670596",
        body=message.body
    )
    return twilio_response.sid


def check_product_exists(product_id):
    exists = db.session.query(Product.id).filter_by(id=product_id).first() is not None
    if not exists:
        raise ServiceError('Product ID provided does not exist', status_code=404)

    return True
